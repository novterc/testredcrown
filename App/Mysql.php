<?php 

Class Mysql
{
	private static $instance;
	protected $mysqlLink;
	protected $config;
	protected $defaultConfigName;

	private function __clone() {}
	private function __construct() {}
	private function __wakeup() {}

	public static function instance($config, $defaultConfigName)
	{
		if(is_null(self::$instance)){
			self::$instance = new self();
			self::$instance->config = $config;
			self::$instance->defaultConfigName = $defaultConfigName;
		}

		return self::$instance;
	}

	public static function get()
	{
		return self::$instance;
	}

	public function getLink($configName=false)
	{	
		if($configName === false){
			$configName = $this->defaultConfigName;
		}

		if(empty($this->config[$configName]))
			App::get()->abort('not found configname:'.$configName);

		$config = $this->config[$configName];

		if(empty($this->mysqlLink[$configName])){
			$link = mysqli_connect( 
				$config['localhost'],
				$config['user'],
				$config['password'],
				$config['basename']
			);

			if (!$link) { 
			   App::get()->abort('Error connect to database:'.mysqli_connect_error(), 1006); 
			} 

			$this->mysqlLink[$configName] = $link;	
			$this->setCharsetUtf8($configName);	
		}
		
		return $this->mysqlLink[$configName];
	}

	public function query($query, $configName=false)
	{
		$link = $this->getLink($configName);
		return mysqli_query($link, $query);
	}

	public function getItemQuery($query, $configName=false)
	{
		$result = $this->query($query, $configName);
		$item = $result->fetch_assoc();
		return $item;
	}

	public function getArrayQuery($query, $configName=false)
	{
		$result = $this->query($query, $configName);
		$arrayResult = [];
		while($mres = $result->fetch_assoc()){
			$arrayResult[] = $mres;
		}
		return $arrayResult;
	}

	public function getLastId($configName=false)
	{
		$link = $this->getLink($configName);
		return mysqli_insert_id($link);
	}

	public function esc($str, $configName=false)
	{
		$link = $this->getLink($configName);
		return mysqli_escape_string($link, $str);
	}

	public function setCharsetUtf8($configName=false)
	{
		$link = $this->getLink($configName);
		$link->query("SET CHARACTER SET 'utf8'");
	}

}
