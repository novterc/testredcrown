<?php

class RowMapperMysql
{
	protected $mysql;
	protected $tableName = 'rows';

	public function __construct()
	{
		$this->mysql = Mysql::get();
	}

	public function initEmpty()
	{
		$this->mysql->query("DROP TABLE `{$this->tableName}`");
		$this->mysql->query("CREATE TABLE `rows` (
								`id` INT NOT NULL AUTO_INCREMENT,
								`name` VARCHAR(250) NULL,
								`status` INT NULL,
								PRIMARY KEY (`id`)
							)
							COLLATE='utf8_general_ci'
							ENGINE=InnoDB;");
	}

	public function create($attrs)
	{
		$keys = array_keys($attrs);
		$keys = array_map(function($item){ return '`'.$item.'`';}, $keys);
		$keysStr = implode(',', $keys);

		$values = array_values($attrs);
		$mysql = $this->mysql;
		$values = array_map(function($item) use($mysql){ return '\''.$mysql->esc($item).'\'';}, $values);
		$valuesStr = implode(',', $values);

		$this->mysql->query("INSERT INTO `{$this->tableName}` ({$keysStr}) VALUES ({$valuesStr})");
	}

	public function updateById($id, $attrs)
	{ 
		$mysql = $this->mysql;
		$sets = [];
		foreach ($attrs as $key => $value) {
			$value = $this->mysql->esc($value);
			$sets[] = "`{$key}`='{$value}'";
		}
		$setsStr = implode(',', $sets);
		$id = intval($id);
		$this->mysql->query("UPDATE `{$this->tableName}` SET {$setsStr} WHERE `id`='{$id}' ");
	}

	public function checkData()
	{
		$result = $this->mysql->query("SELECT 1 FROM `{$this->tableName}` LIMIT 1");
		if($result !== false && $result->num_rows !== 0)
			return true;
		else
			return false;
	}

	public function checkConnect()
	{
		try {
			$this->mysql->getLink();
			return true;
		} catch(Exception $e) {
			if($e->getCode() === 1006){
				return false;
			} else {
				throw $e;				
			}
		}
	}

	public function getRandom()
	{
		$mysql = Mysql::get();
		$data = $mysql->getItemQuery("SELECT * FROM `{$this->tableName}` ORDER BY RAND() LIMIT 1");
		return $this->getObjByDate($data);
	}

	protected function getObjByDate($data)
	{
		$obj = new Row($this);
		foreach ($data as $key => $value) {
			$obj->$key = $value;
		}
		return $obj;
	}
		
}
