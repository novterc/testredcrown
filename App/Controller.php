<?php

class Controller
{

	public function main()
	{
		$rowMapper = new RowMapperMysql();
		if($rowMapper->checkConnect()){
			if(!$rowMapper->checkData())
				$this->loadCSVData();
		} else {
			$rowMapper = new RowMapperCSV();
		}

		$row = $rowMapper->getRandom();
		$row->toggleStatus();
		$row->save();

		echo "\r\n".$row->name."\t".$row->status."\r\n";
	}

	public function loadCSVData()
	{
		$rowMapperCSV = new RowMapperCSV();
		$data = $rowMapperCSV->getAllData();
		
		$rowMapperMysql = new RowMapperMysql();
		$rowMapperMysql->initEmpty();
		foreach ($data as $item) {
			$rowMapperMysql->create([
				'name' => $item[0],
				'status' => $item[1],
			]);
		}
	}

}
