<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require __DIR__ .'/App.php';
require __DIR__ .'/Controller.php';
require __DIR__ .'/RowMapperMysql.php';
require __DIR__ .'/RowMapperCSV.php';
require __DIR__ .'/Mysql.php';
require __DIR__ .'/Row.php';

$App = App::get();
