<?php

class RowMapperCSV
{
	
	public function __construct()
	{
		$this->filePath = dirname(__DIR__).'/data.csv';
	}

	public function getAllData()
	{
		$data = $this->readFile();
		unset($data[0]);
		return $data;
	}

	public function getRandom()
	{
		$data = $this->readFile();
		unset($data[0]);
		$randKey = array_rand($data);
		$item = $data[$randKey];
		
		$obj = new Row($this);
		$obj->name = $item[0];
		$obj->status = $item[1];
		
		return $obj;
	}

	public function updateById()
	{}

	protected function readFile()
	{
		$resData = [];
		if (($handle = fopen($this->filePath, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $num = count($data);
		        $resData[] = $data;
		    }
		    fclose($handle);
		}
		return $resData;
	}

}
