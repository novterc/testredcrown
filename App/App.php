<?php

class App
{
	private static $instance;

	private function __clone() {}
	private function __wakeup() {}
	private function __construct() {
		$this->config = include 'config.php';
		Mysql::instance($this->conf('mysql'), 'default');
	}

	public static function get()
	{
		if(is_null(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function abort($str, $code = 0)
	{
		throw new Exception($str, $code);
	}

	public function conf($path) 
	{
		$keyArr = explode('.', $path);
		$config = $this->config;
		foreach ($keyArr as $key) {
			if(!isset($key))
				$this->abort('undefined config path:'.$path);
			$config = $config[$key];
		}
		return $config;
	}

	public function run()
	{
		$controller = new Controller();
		$controller->main();
	}

}
