<?php

class Row
{
	public $id;
	public $name;
	public $status;
	protected $mapper;

	public function __construct($mapper)
	{
		$this->mapper = $mapper;
	}

	public function save()
	{
		$this->mapper->updateById($this->id, [ 
			'status' => $this->status,
			'name' => $this->name,
		]);
	}

	public function toggleStatus()
	{
		if($this->status == '1')
			$this->status = '0';
		else
			$this->status = 1;
	}

}
